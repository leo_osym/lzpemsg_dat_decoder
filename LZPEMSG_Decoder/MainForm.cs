﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LZPEMSG_Decoder
{
    public partial class MainForm : Form
    {
        public string Line;
        public string Number;
        List<string> aplus = new List<string>();
        List<string> aminus = new List<string>();
        List<string> rplus = new List<string>();
        List<string> rminus = new List<string>();

        List<Energy> SumEnergy = new List<Energy>();
        List<Energy> MonthEnergy = new List<Energy>();
        List<DateTime> mtimes = new List<DateTime>();
        Energy counted;
        List<Energy> Counted = new List<Energy>();
        public DateTime Dt;
        public MainForm()
        {
            InitializeComponent();
        }

        private void buttonOpen_Click(object sender, EventArgs e)
        {
            listView.Items.Clear();
            comboBoxDate.Items.Clear();
            comboBoxSum.Items.Clear();
            SumEnergy.Clear();
            MonthEnergy.Clear();
            openFileDialog.DefaultExt = "dat";
            openFileDialog.Filter = "All Files (*.*)|*.*| Data files (*.dat)|*.dat";
            openFileDialog.FilterIndex = 2;
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                using (StreamReader reader = new StreamReader(openFileDialog.OpenFile(), Encoding.UTF8))
                {
                    while ((Line = reader.ReadLine()) != null)
                    {
                        if (Line[0].Equals('D'))
                        {
                            Dt = DateTime.ParseExact(Line.Split(new string[] { "(", ")" }, StringSplitOptions.RemoveEmptyEntries)[1], "yyMMddHHmmss", CultureInfo.CurrentCulture);
                        }
                        if (Line[0].Equals('0'))
                        {
                            Number = Line.Split(new string[] { "(", ")", "&" }, StringSplitOptions.RemoveEmptyEntries)[1];
                        }
                        if (Line[0].Equals('8') & Line[2].Equals('0'))
                            SaveSum();

                        if (Line[0].Equals('7'))
                            SaveSum();
                    }
                }
            }
            for (int i =0; i< SumEnergy.Count; i++)
            {
                comboBoxSum.Items.Add("["+i+"] "+SumEnergy[i].Date.ToString("yyyy-MM-dd, HH:mm:ss"));
            }
            //listView.Items.Clear();
            if(SumEnergy[0]!=null)
                Print(SumEnergy[0]);
            //Print(MonthEnergy);
            //listViewShow();
        }

        private void SaveSum()
        {

            if (Line[1].Equals('0'))
            {
                aplus = Line.Split(new string[] { "3D", "(", ")" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                aplus.RemoveAt(0);
            }
            if (Line[1].Equals('1'))
            {
                aminus = Line.Split(new string[] { "3D", "(", ")" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                aminus.RemoveAt(0);
            }
            if (Line[1].Equals('2'))
            {
                rplus = Line.Split(new string[] { "3D", "(", ")" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                rplus.RemoveAt(0);
            }
            if (Line[1].Equals('3'))
            {
                rminus = Line.Split(new string[] { "3D", "(", ")" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                rminus.RemoveAt(0);
                if (Line[0].Equals('8'))
                {
                    SumEnergy.Add(new Energy
                    {
                        Date = Dt,
                        Number = Number,
                        Aplus = EnergyDecoder.Decode(aplus),
                        Aminus = EnergyDecoder.Decode(aminus),
                        Rplus = EnergyDecoder.Decode(rplus),
                        Rminus = EnergyDecoder.Decode(rminus)
                    });
                }
                if (Line[0].Equals('7'))
                {
                    MonthEnergy.Add(new Energy
                    {
                        Date = Line[2].Equals('0') 
                            ? Dt = new DateTime(Dt.Year, Dt.Month, Dt.Day, Dt.Hour, Dt.Minute, 0) 
                            : Dt = new DateTime(Dt.Year, Dt.Month, Dt.Day, Dt.Hour, Dt.Minute, Dt.Second + 1),
                        Number = Number,
                        Aplus = EnergyDecoder.Decode(aplus),
                        Aminus = EnergyDecoder.Decode(aminus),
                        Rplus = EnergyDecoder.Decode(rplus),
                        Rminus = EnergyDecoder.Decode(rminus)
                    });
                }

            }
        }

        private void listViewShow() // by now is not used
        {
            listView.Items.Clear();
            Print(SumEnergy);
            //Print(MonthEnergy);
        }

        private void Print(List<Energy> energy)
        {
            string format = "00000.000";
            foreach (var en in energy)
            {
                double[] sum = { 0, 0, 0, 0 };
                ListViewItem item1 = new ListViewItem { Text = "Chanel A+, kWt*h" };
                ListViewItem item2 = new ListViewItem { Text = "Chanel A-, kWt*h" };
                ListViewItem item3 = new ListViewItem { Text = "Chanel R+, kvar*h" };
                ListViewItem item4 = new ListViewItem { Text = "Chanel R-, kvar*h" };
                for (int i = 0; i < 4; i++)
                {
                    sum[0] += en.Aplus[i] / 1000;
                    sum[1] += en.Aminus[i] / 1000;
                    sum[2] += en.Rplus[i] / 1000;
                    sum[3] += en.Rminus[i] / 1000;
                    item1.SubItems.Add((en.Aplus[i] / 1000).ToString(format));
                    item2.SubItems.Add((en.Aminus[i] / 1000).ToString(format));
                    item3.SubItems.Add((en.Rplus[i] / 1000).ToString(format));
                    item4.SubItems.Add((en.Rminus[i] / 1000).ToString(format));
                }
                item1.SubItems.Add(sum[0].ToString(format));
                item2.SubItems.Add(sum[1].ToString(format));
                item3.SubItems.Add(sum[2].ToString(format));
                item4.SubItems.Add(sum[3].ToString(format));

                listView.Items.Add(item1);
                listView.Items.Add(item2);
                listView.Items.Add(item3);
                listView.Items.Add(item4);
                ListViewItem item5 = new ListViewItem { Text = en.Date.ToString("yyyy-MM-dd, HH:mm:ss") };
                item5.SubItems.Add(Number);
                listView.Items.Add(item5);
            }

            //textBoxDate.Text = "Date: " + Dt.ToString("yyyy-MM-dd, HH:mm:dd");
        }

        private void Print(Energy energy)
        {
            string format = "00000.000";

            double[] sum = { 0, 0, 0, 0 };
            ListViewItem item1 = new ListViewItem { Text = "Chanel A+, kWt*h" };
            ListViewItem item2 = new ListViewItem { Text = "Chanel A-, kWt*h" };
            ListViewItem item3 = new ListViewItem { Text = "Chanel R+, kvar*h" };
            ListViewItem item4 = new ListViewItem { Text = "Chanel R-, kvar*h" };
            for (int i = 0; i < 4; i++)
            {
                sum[0] += energy.Aplus[i] / 1000;
                sum[1] += energy.Aminus[i] / 1000;
                sum[2] += energy.Rplus[i] / 1000;
                sum[3] += energy.Rminus[i] / 1000;
                item1.SubItems.Add((energy.Aplus[i] / 1000).ToString(format));
                item2.SubItems.Add((energy.Aminus[i] / 1000).ToString(format));
                item3.SubItems.Add((energy.Rplus[i] / 1000).ToString(format));
                item4.SubItems.Add((energy.Rminus[i] / 1000).ToString(format));
            }
            item1.SubItems.Add(sum[0].ToString(format));
            item2.SubItems.Add(sum[1].ToString(format));
            item3.SubItems.Add(sum[2].ToString(format));
            item4.SubItems.Add(sum[3].ToString(format));

            listView.Items.Add(item1);
            listView.Items.Add(item2);
            listView.Items.Add(item3);
            listView.Items.Add(item4);
            ListViewItem item5 = new ListViewItem { Text = energy.Date.ToString("yyyy-MMM-dd, HH:mm") };
            item5.SubItems.Add(Number);
            listView.Items.Add(item5);

            //textBoxDate.Text = "Date: " + Dt.ToString("yyyy-MM-dd, HH:mm:ss");
        }

        private void comboBoxSum_DropDownClosed(object sender, EventArgs e)
        {
            if (comboBoxSum.SelectedItem != null)
            {
                int sumIndex = Int32.Parse(comboBoxSum.SelectedItem.ToString().Split(new string[] { "[", "]" }, StringSplitOptions.RemoveEmptyEntries)[0]);
                listView.Items.Clear();
                Print(SumEnergy[sumIndex]);

                comboBoxDate.Items.Clear();
                for (int i = 0; i < MonthEnergy.Count; i++)
                {
                    if(MonthEnergy[i].Date.Year == SumEnergy[sumIndex].Date.Year &
                        MonthEnergy[i].Date.Month == SumEnergy[sumIndex].Date.Month &
                        MonthEnergy[i].Date.Day == SumEnergy[sumIndex].Date.Day &
                        MonthEnergy[i].Date.Hour == SumEnergy[sumIndex].Date.Hour)
                    {
                        comboBoxDate.Items.Add("[" + i + "] "
                            + SetDate(i, MonthEnergy[i].Date).ToString("yyyy MMM")+" 01, 00:00");
                        //+ MonthEnergy[i].Date.ToString("yyyy-MM-dd, HH:mm:ss"));
                        mtimes.Add(MonthEnergy[i].Date);
                    }
                }
            }
        }
        private DateTime SetDate(int i, DateTime date)
        {
            DateTime dt = date.AddMonths(-i);
            return dt;
        }

        private void comboBoxDate_DropDownClosed(object sender, EventArgs e)
        {
            int ind = 0;
            if (comboBoxDate.SelectedItem != null)
            {
                int monIndex = Int32.Parse(comboBoxDate.SelectedItem.ToString().Split(new string[] { "[", "]" }, StringSplitOptions.RemoveEmptyEntries)[0]);
                int sumIndex = Int32.Parse(comboBoxSum.SelectedItem.ToString().Split(new string[] { "[", "]" }, StringSplitOptions.RemoveEmptyEntries)[0]);
                counted = SumEnergy[sumIndex].Clone();

                for (int i = 0; i < MonthEnergy.Count; i++)
                {
                    if (i >= 0 && MonthEnergy[i].Date == mtimes[0]) ind = i;
                }
                for (int i = ind; i <= ind + monIndex; i++)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        counted.Aplus[j] -= MonthEnergy[i].Aplus[j];
                        counted.Aminus[j] -= MonthEnergy[i].Aminus[j];
                        counted.Rplus[j] -= MonthEnergy[i].Rplus[j];
                        counted.Rminus[j] -= MonthEnergy[i].Rminus[j];
                    }
                }
                counted.Date = new DateTime(counted.Date.Year, counted.Date.Month, 1, 0, 0, 0);
                counted.Date = counted.Date.AddMonths(-monIndex);
                listView.Items.Clear();
                Print(counted);
            }
        }

        private void buttonExport_Click(object sender, EventArgs e)
        {
            Export export = new Export();
            export.ExportToExcel(Counted);
            Counted.Clear();
        }

        private void buttonAddToExport_Click(object sender, EventArgs e)
        {
            Counted.Add(counted);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Do you want to exit?","Close",MessageBoxButtons.OKCancel) == DialogResult.OK)
                Close();
        }
    }
}
