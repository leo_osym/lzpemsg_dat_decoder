﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Core;
using Excel = Microsoft.Office.Interop.Excel;

namespace LZPEMSG_Decoder
{
    public class Energy
    {
        public DateTime Date { get; set; }
        public string Number { get; set; }
        public List<double> Aplus { get; set; }
        public List<double> Aminus { get; set; }
        public List<double> Rplus { get; set; }
        public List<double> Rminus { get; set; }

        public Energy Clone()
        {
            Energy temp = new Energy();
            for(int i =0;i<4;i++)
            {
                temp.Date = Date;
                temp.Number = Number;
                temp.Aplus = new List<double>(Aplus);
                temp.Aminus = new List<double>(Aminus);
                temp.Rplus = new List<double>(Rplus);
                temp.Rminus = new List<double>(Rminus);
            }
            return temp;
        }
    }
    static class EnergyDecoder
    {
        public static List<double> Decode(List<string> list)
        {
            List<double> listDouble = new List<double>(list.Count);
            for (int i = 0; i < list.Count; i++)
            {
                list[i] = list[i].Substring(6, 2) +
                    list[i].Substring(4, 2) +
                    list[i].Substring(2, 2) +
                    list[i].Substring(0, 2);
                listDouble.Add(Convert.ToInt64(list[i]));
            }
            return listDouble;
        }

    }
    class Export
    {
        public void ExportToExcel(List<Energy> Energy)
        {
            var excelApp = new Excel.Application();
            excelApp.Visible = true;

            string path = System.IO.Directory.GetCurrentDirectory() + "/temp_1tar.xls";
            excelApp.Workbooks.Add(path);

            Excel._Worksheet workSheet = (Excel.Worksheet)excelApp.ActiveSheet;
            int j = 4;
            for(int i =0; i<Energy.Count; i++)
            {
                workSheet.Cells[j, "B"] = i+1;
                workSheet.Cells[j, "D"] = Energy[i].Number;
                workSheet.Cells[j++, "E"] = Energy[i].Aplus[0] / 1000;
                workSheet.Cells[j, "D"] = "01-"+Energy[i].Date.ToString("MM-yyyy") + ", 00:00";
                workSheet.Cells[j++, "E"] = Energy[i].Aminus[0] / 1000;
                workSheet.Cells[j++, "E"] = Energy[i].Rplus[0] / 1000;
                workSheet.Cells[j++, "E"] = Energy[i].Rminus[0] / 1000;
            }
        }
        public void ExportToExcel(Energy energy)
        {
            var excelApp = new Excel.Application();
            // Make the object visible.
            excelApp.Visible = true;

            // Create a new, empty workbook and add it to the collection returned 
            // by property Workbooks. The new workbook becomes the active workbook.
            // Add has an optional parameter for specifying a praticular template. 
            // Because no argument is sent in this example, Add creates a new workbook. 
            string path = System.IO.Directory.GetCurrentDirectory() + "/temp_1tar.xls";
            excelApp.Workbooks.Add(path);

            // This example uses a single workSheet. The explicit type casting is
            // removed in a later procedure.
            Excel._Worksheet workSheet = (Excel.Worksheet)excelApp.ActiveSheet;
            workSheet.Cells[3, "E"] = "Показания на \n01-" + energy.Date.ToString("MM-yy") + ", 00:00";
            workSheet.Cells[4, "D"] = energy.Number;
            workSheet.Cells[4, "E"] = energy.Aplus[0] / 1000;
            workSheet.Cells[5, "E"] = energy.Aplus[1] / 1000;
            workSheet.Cells[6, "E"] = energy.Aplus[2] / 1000;
            workSheet.Cells[7, "E"] = (energy.Aplus[0] + energy.Aplus[1] + energy.Aplus[2] + energy.Aplus[3]) / 1000;
            workSheet.Cells[8, "E"] = energy.Aminus[0] / 1000;
            workSheet.Cells[9, "E"] = energy.Aminus[1] / 1000;
            workSheet.Cells[10, "E"] = energy.Aminus[2] / 1000;
            workSheet.Cells[11, "E"] = (energy.Aminus[0] + energy.Aminus[1] + energy.Aminus[2] + energy.Aminus[3]) / 1000;
            workSheet.Cells[12, "E"] = energy.Rplus[0] / 1000;
            workSheet.Cells[13, "E"] = energy.Rplus[1] / 1000;
            workSheet.Cells[14, "E"] = energy.Rplus[2] / 1000;
            workSheet.Cells[15, "E"] = (energy.Rplus[0] + energy.Rplus[1] + energy.Rplus[2] + energy.Rplus[3]) / 1000;
            workSheet.Cells[16, "E"] = energy.Rminus[0] / 1000;
            workSheet.Cells[17, "E"] = energy.Rminus[1] / 1000;
            workSheet.Cells[18, "E"] = energy.Rminus[2] / 1000;
            workSheet.Cells[19, "E"] = (energy.Rminus[0] + energy.Rminus[1] + energy.Rminus[2] + energy.Rminus[3]) / 1000;
        }
    }

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
